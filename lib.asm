%define SYSTEM_READ 0
%define SYSTEM_WRITE 1
%define SYSTEM_EXIT 60
%define STDOUT 1
%define STDIN 0
%define STDERR 2

section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_string



; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYSTEM_EXIT     
    syscall


string_length:
    xor rax, rax
    .main_loop:
        cmp byte [rdi + rax], 0
        je .return
        inc rax
        jmp .main_loop
    .return:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov  rdx, rax
    pop rsi
    mov  rax, SYSTEM_WRITE
    mov  rdi, STDOUT
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    mov rax, rdi
    push rax
    mov rsi, rsp
    mov rdx, 1
    mov rax, SYSTEM_WRITE
    mov rdi, STDOUT
    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    xor rcx, rcx
    mov rsi, 10
    xor rdx, rdx
    .loop:
        inc rcx
        xor rdx, rdx
        div rsi
        add dl, "0"
        push rdx
        test rax, rax
        jne .loop
    .print_loop:
        pop rdi
        push rcx
        call print_char
        pop rcx
        dec rcx
        test rcx, rcx
        jne .print_loop
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .bigger_or_equal_0
    .smaller_than_0:
        push rdi
        mov rdi, '-'
        call print_char
        xor rax, rax
        pop rdi
        sub rax, rdi
        mov rdi, rax
        call print_uint
        ret
    .bigger_or_equal_0:
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor rax, rax
    xor rdx, rdx
    .loop:
        movzx rax, byte [rdi+rcx]      
        movzx rdx, byte [rsi+rcx]
        cmp rax, rdx
        jne .not_equals
        cmp rax, 0
        je .equals
        inc rcx
        jmp .loop
    .not_equals:
        mov rax, 0
        ret
    .equals:
        mov rax, 1
        ret 


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    sub rsp, 1
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    xor rax, rax
    syscall
    cmp rax, 0
    je .return
    movzx rax, byte [rsp]
    add rsp, 1
    ret
    .return:
        add rsp, 1
        xor rax, rax
        ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
  push rdi
  .main_loop:
        push rdi
        push rsi
        push rdx
        call read_char
        pop rdx
        pop rsi
        pop rdi
        cmp al, 0xA
        je .space_check
        cmp al, 0x9
        je .space_check
        cmp al, 0x20
        je .space_check
        test al, al
        je .end
        jmp .append;
    .space_check:
        test rdx, rdx
        je .main_loop
        jmp .end
    .append:
        mov byte[rdi+rdx], al
        inc rdx
        cmp rsi, rdx
        je .exception 
        jmp .main_loop
    .end:
        pop rax
        mov byte[rdi+rdx], 0
        ret
    .exception:
        pop rdi
        xor rax, rax
        ret

read_string:
  push rdi
  .main_loop:
        push rdi
        push rsi
        push rdx
        call read_char
        pop rdx
        pop rsi
        pop rdi
        test al, al
        je .end
    .append:
        mov byte[rdi+rdx], al
        inc rdx
        cmp rsi, rdx
        je .exception 
        jmp .main_loop
    .end:
        pop rax
        mov byte[rdi+rdx], 0
        ret
    .exception:
        pop rdi
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:
    xor rcx, rcx
    xor rax, rax
    mov rsi, 10
    xor r9, r9
    .main_loop:
      mov cl, [rdi+r9]
      sub cl, `0`
      cmp cl, 0
      jl .return
      cmp cl, 9
      jg .return
      inc r9
      mul rsi
      add rax, rcx
      jmp .main_loop
    .return:
      mov rdx, r9
      ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rsi, rsi
    cmp byte[rdi], '+'
    je .pos
    cmp byte[rdi], '-'
    je .neg
    call parse_uint
    ret
    .neg:
        inc rdi
        call parse_uint
        neg rax
        jmp .return
    .pos:
        inc rdi
        call parse_uint
    .return:
        inc rdx
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
    .main_loop:
        test rdx, rdx
        je .exception
        mov cl, byte [rdi+rax]
        mov byte [rsi+rax], cl
        test cl, cl
        je .return
        inc rax
        dec rdx
        jmp .main_loop
    .return:
        ret
    .exception:
        xor rax, rax
        ret    
