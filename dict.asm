%include "lib.inc"

global find_word

section .text

find_word:
	push r14
	push r15
	mov r14, rdi
	mov r15, rsi
	.main_loop:
		test r15, r15
		je .not_found
		mov rdi, r14
		lea rsi, [r15 + 8]
		call string_equals
		test rax, rax
		jne .found
		mov r15, [r15]
		jmp .main_loop	
		
	.not_found:
		xor rax, rax
		jmp .quit
	.found:
		mov rax, r15
	.quit:
		pop r15
		pop r14
		ret
