%include "lib.inc"
%include "words.inc"
%include "dict.inc"


%define buf_len 256
%define write 1
%define std_err 2
section .bss
buf: resb buf_len

section .rodata
too_long_msg: db "length of the input string is greater than 256", 0
not_found_msg: db "no such word in the dictionary", 0

section .text
global _start

write_err:
	push rdi
	call string_length
	mov rdx, rax				
	mov rax, write				
	mov rdi, std_err				
	syscall
	pop rdi
	ret

_start:
	mov rdi, buf
	mov rsi, buf_len
	call read_string
	test rax, rax
	je .too_long
	
	mov rdi, buf
	mov rsi, last_elem 
	call find_word
	test rax, rax
	je .not_found
	
	lea rdi, [rax+8]
	call string_length
	lea rdi, [rdi+rax+1]
	call print_string
	call exit

	.too_long:
		mov rdi, too_long_msg
		call write_err
		call exit

	.not_found:
		mov rdi, not_found_msg
		call write_err
		call exit



