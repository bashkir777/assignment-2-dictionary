.PHONY: clean
ASM=nasm
ASMFLAGS=-f elf64 -o
TOCLEAN=program main.o lib.o dict.o
PYTHON=python2
TEST=test.py
LD=ld -o
main.o: main.asm lib.inc words.inc dict.inc
	$(ASM) $(ASMFLAGS) $@ $<

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) $@ $<

dict.o: dict.asm lib.inc
	$(ASM) $(ASMFLAGS) $@ $<

program: main.o lib.o dict.o
	$(LD) $@ $^ 

clean:
	rm $(TOCLEAN)
test:
	$(PYTHON) $(TEST)
