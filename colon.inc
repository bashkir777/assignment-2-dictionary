%define last_elem 0

%macro colon 2
    %ifid %2
        %ifstr %1
            %2: dq last_elem
            %define last_elem %2
            db %1, 0
        %else
            %error "First argument should be a string"
        %endif
    %else
        %error "Second argument should be a label"
    %endif
%endmacro
