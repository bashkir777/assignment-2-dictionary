import subprocess



input_strings = ["first", "second", "third", "to" + "o"*260+"long", "undefined_key"]

expected_stdouts = ["first word description", "second word description", "third word description", "", ""]

expected_stderrs = ["", "", "", "length of the input string is greater than 255", "no such word in the dictionary"]


failed_tests_counter = 0

for i in range(0, len(input_strings)):
    
    process = subprocess.Popen("./program", stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    stdout, stderr = process.communicate(input_strings[i].encode())

    recieved_stdout = stdout.decode()
    recieved_stderr = stderr.decode()
    
    if not (recieved_stdout == expected_stdouts[i] and recieved_stderr == expected_stderrs[i]):
        failed_tests_counter += 1
        print("Wrong stdout or stderr on input: " + input_strings[i]+ " expected stdout: "+ expected_stdouts[i] + " expected stderr: " + expected_stderrs[i])
        print("your stdout: " + recieved_stdout + " stderr: " + recieved_stderr) 

if failed_tests_counter == 0:
    print("all tests passed")
else:
    print("failures in total: " + str(failed_tests_counter))
	
 
